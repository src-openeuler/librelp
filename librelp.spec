Name:             librelp
Version:          1.11.0
Release:          1
Summary:          An Easy To Use Library For The RELP Protocol
License:          GPLv3+
URL:              http://www.rsyslog.com/
Source0:          http://download.rsyslog.com/librelp/%{name}-%{version}.tar.gz
Requires(post):   glibc
Requires(postun): glibc
BuildRequires:    gnutls-devel >= 1.4.0

%description
Librelp is a convenient RELP protocol library. RELP (for Reliable Event
Logging Protocol) is a general-purpose and extensible logging protocol.

%package devel
Summary:          Development files for librep
Requires:         %{name} = %{version}-%{release} pkgconfig
BuildRequires:    autoconf automake libtool

%description devel
The librelp-devel package includes header files and libraries necessary
for the librelp library.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure --disable-static --disable-tls-openssl
%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYING NEWS README doc/*html
%{_libdir}/librelp.so.*

%files devel
%{_includedir}/*
%{_libdir}/librelp.so
%{_libdir}/pkgconfig/relp.pc

%changelog
* Wed Jun 07 2023 wangkai <13474090681@163.com> - 1.11.0-1
- Update to 1.11.0

* Tue Jan 12 2021 xu_ping<xuping33@huawei.com> - 1.2.16-4
- remove AM_INIT_AUTOMAKE to fix AM_INIT_AUTOMAKE expanded multiple times

* Wed Nov 27 2019 gulining<gulining1@huawei.com> - 1.2.16-3
- Pakcage init
